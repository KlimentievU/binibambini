﻿using UnityEngine;
using System.IO;
using System;

public class FieldGenerator : MonoBehaviour {
	public string[] mapFiles;
	[SerializeField] Vector2 offset = new Vector2(0, 0);
	[SerializeField] GameObject[] tiles;

	public void LoadMap(int mapFileIndex) {
		StreamReader reader = new StreamReader(mapFiles[mapFileIndex]);

		int i        = 0, j = 0;
		var fileData = reader.ReadToEnd().Split('\n');
		offset.y = -((float) fileData.Length - 1) / 2;
		offset.x = -((float) fileData[0].Length - 2) / 2;
		foreach (var row in fileData) {
			j = 0;
			foreach (char col in row) {
				if ((int) Char.GetNumericValue(col) != -1) {
					var pool = GameObject.FindGameObjectWithTag("Pool");
					Instantiate(tiles[(int) Char.GetNumericValue(col)],
						new Vector3(offset.x + transform.position.x + j, offset.y + transform.position.y + i),
						transform.rotation,
						pool.transform);
				}
				j++;
			}
			i++;
		}
		reader.Close();
	}
}
