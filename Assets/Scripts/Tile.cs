﻿using UnityEngine;

public class Tile : MonoBehaviour {
	public GameObject[] instantiable;

	void Start() { Instantiate(instantiable[0], transform.position, transform.rotation, transform); }
}
