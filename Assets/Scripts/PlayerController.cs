﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	private float _speed = 200f;
	private bool _ghostForm;
	private Rigidbody _rigidbody;
	[SerializeField] SpriteRenderer _sprite;

	private void Start() { _rigidbody = GetComponent<Rigidbody>(); }

	private void Update() {
		var x = Input.GetAxis("Horizontal") * Time.deltaTime * _speed;
		var y = Input.GetAxis("Vertical") * Time.deltaTime * _speed;

		_rigidbody.velocity = new Vector3(x, y);
	}

	public void EnterGhostForm() { StartCoroutine(GhostForm()); }

	public void ReceiveDamage() {
		if (!_ghostForm) {
			StartCoroutine(ShowRecieveDamage());
			if (GameManager.Instance.Lives <= 0) GameManager.Instance.GameOver();
		}
	}

	private IEnumerator ShowRecieveDamage() {
		GameManager.Instance.Lives--;
		GameManager.Instance.UpdateUi();
		Color temp = _sprite.color;
		_sprite.color = Color.red;
		yield return new WaitForSeconds(0.1f);
		_sprite.color = temp;
	}

	private IEnumerator GhostForm() {
		_ghostForm    = true;
		_sprite.color = Color.black;
		yield return new WaitForSeconds(5);
		_sprite.color = Color.white;
		_ghostForm    = false;
	}
}
