using PathFind;
using UnityEngine;
using Grid = PathFind.Grid;

public class GameInitialization : MonoBehaviour {
	[SerializeField] private FieldGenerator _fieldGenerator;
	[SerializeField] private UiManager _uiManager;

	private void Start() {
		var fieldGenerator = Instantiate(_fieldGenerator);

		var pathManager = new GameObject();
		pathManager.name = "PathManager";
		var grid = pathManager.AddComponent<Grid>();
		grid.UnwalkableMask = LayerMask.GetMask("Unwalkable");
		var pathfinding        = pathManager.AddComponent<Pathfinding>();
		var pathRequestManager = pathManager.AddComponent<PathRequestManager>();

		var gameManager = new GameObject().AddComponent<GameManager>();
		gameManager.name = "GameManager";
		_uiManager.Create(gameManager);
		gameManager.Init(grid, fieldGenerator, pathRequestManager, _uiManager, pathfinding);
	}
}
