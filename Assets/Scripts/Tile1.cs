﻿using UnityEngine;

public class Tile1 : Tile {
	public void SpawnBonus(int bonus) {
		var pool = GameObject.FindGameObjectWithTag("Pool");
		Instantiate(instantiable[bonus], transform.position, transform.rotation, pool.transform);
	}
}
