﻿using UnityEngine;

namespace Bonuses {
	public class GostBonus : Bonus {
		private void OnTriggerEnter(Collider other) {
			if (other.gameObject.CompareTag("Player")) {
				other.gameObject.GetComponent<PlayerController>().EnterGhostForm();
				GameManager.Instance.AddToEmptyTilesList(Tile1);
				GameManager.Instance.UpdateUi();
				Destroy(gameObject);
			}
		}
	}
}
