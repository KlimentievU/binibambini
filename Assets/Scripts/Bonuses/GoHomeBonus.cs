﻿using UnityEngine;

namespace Bonuses {
	public class GoHomeBonus : Bonus {
		private void OnTriggerEnter(Collider other) {
			if (other.gameObject.CompareTag("Player")) {
				GameManager.Instance.Enemies[Random.Range(0, GameManager.Instance.Enemies.Count)].GoHome();
				GameManager.Instance.AddToEmptyTilesList(Tile1);
				GameManager.Instance.UpdateUi();
				Destroy(gameObject);
			}
		}
	}
}
