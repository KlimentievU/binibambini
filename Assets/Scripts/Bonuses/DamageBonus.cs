﻿using UnityEngine;

namespace Bonuses {
	public class DamageBonus : Bonus {
		private void OnTriggerEnter(Collider other) {
			if (other.gameObject.CompareTag("Player")) {
				other.gameObject.GetComponent<PlayerController>().ReceiveDamage();
				GameManager.Instance.AddToEmptyTilesList(Tile1);
				GameManager.Instance.UpdateUi();
				Destroy(gameObject);
			}
		}
	}
}
