﻿using UnityEngine;

namespace Bonuses {
	public class Coin : Bonus {
		private void Start() { GameManager.Instance.CoinsToWin++; }

		private void OnCollisionEnter(Collision collision) {
			if (collision.gameObject.CompareTag("Player")) {
				GameManager.Instance.CoinCollected();
				GameManager.Instance.AddToEmptyTilesList(Tile1);
				Destroy(gameObject);
			}
		}
	}
}
