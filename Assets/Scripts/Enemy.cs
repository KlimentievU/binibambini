﻿using System.Collections;
using PathFind;
using UnityEngine;

public class Enemy : Unit {
	[SerializeField] private SpriteRenderer _sprite;
	private PlayerController _player;

	private bool _home;
	private bool _slow;

	private void Start() {
		GameManager.Instance.Enemies.Add(this);
		_player = FindObjectOfType<PlayerController>();
		target  = _player.transform;
		StartCoroutine(UpdatePath());
	}

	public void GoHome() {
		if (!_home)
			StartCoroutine(WayToHome());
	}

	public void Slow() {
		if (!_slow)
			StartCoroutine(Slowing());
	}

	private IEnumerator Slowing() {
		_slow        = true;
		_sprite.color = Color.yellow;
		float temp = Speed;
		Speed /= 2;
		yield return new WaitForSeconds(5);
		Speed         = temp;
		_sprite.color = Color.white;
		_slow        = false;
	}

	private IEnumerator WayToHome() {
		_home       = true;
		_sprite.color = Color.red;
		target        = transform.parent;
		yield return new WaitForSeconds(3);
		_sprite.color = Color.white;
		target        = _player.transform;
		_home       = false;
	}

	private void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag("Player")) _player.ReceiveDamage();
	}
}
