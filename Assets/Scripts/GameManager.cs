﻿using System.Collections;
using System.Collections.Generic;
using PathFind;
using UnityEngine;
using UnityEngine.SceneManagement;
using Grid = PathFind.Grid;

public class GameManager : MonoBehaviour {
	public static GameManager Instance;

	public int Lives = 3;
	public int Level = 0;
	public int CoinsToWin = 0;
	public List<Enemy> Enemies = new List<Enemy>();
	public bool Game;

	private Grid _grid;
	private FieldGenerator _fGenerator;
	private PathRequestManager _pathRequestManager;
	private UiManager _uiManager;
	private Pathfinding _pathfinding;
	private int _score = 0;
	private int _currentCoins = 0;
	private List<Tile1> emptyTiles = new List<Tile1>();

	private GameObject _pool;

	public void Init(Grid grid,
		FieldGenerator fGenerator,
		PathRequestManager pathRequestManager,
		UiManager uiManager,
		Pathfinding pathfinding) {
		Instance            = this;
		_grid               = grid;
		_fGenerator         = fGenerator;
		_pathRequestManager = pathRequestManager;
		_uiManager          = uiManager;
		_pathfinding        = pathfinding;
		_pool               = GameObject.FindGameObjectWithTag("Pool");

		_uiManager.Init();
		_pathfinding.Init(_pathRequestManager, _grid);
		_pathRequestManager.Init();
		StartLevel();
	}

	private void StartLevel() {
		if (_pool.transform.childCount > 0)
			for (int i = 0; i < _pool.transform.childCount; i++)
				Destroy(_pool.transform.GetChild(i).gameObject);
		emptyTiles.Clear();
		Enemies.Clear();
		StartCoroutine(Load());
	}

	IEnumerator Load() {
		yield return new WaitForSeconds(0.2f);
		_fGenerator.LoadMap(Level);
		_grid.Init();
		CoinsToWin    = 0;
		_currentCoins = 0;
		UpdateUi();
		if (!IsInvoking("SpawnBonuses")) InvokeRepeating("SpawnBonuses", 0f, 5f);
	}


	private void NextLevel() {
		if (_fGenerator.mapFiles.Length > Level + 1) {
			Level++;
			StartLevel();
			_uiManager.HideAllMenus();
		}
		else _uiManager.WinScreen();
	}


	public void CoinCollected() {
		_currentCoins++;
		_score += 10;
		UpdateUi();
		if (_currentCoins >= CoinsToWin) NextLevel();
	}

	public void UpdateUi() {
		_uiManager.SetScore("Score : " +
		                    _score +
		                    "\n" +
		                    "Coins : " +
		                    _currentCoins +
		                    "/" +
		                    CoinsToWin +
		                    "\n" +
		                    "Level : " +
		                    (Level + 1) +
		                    "\nHP : " +
		                    Lives);
	}

	public void AddToEmptyTilesList(Tile1 tile) { emptyTiles.Add(tile); }

	public void RemoveFromEmptyTilesList(Tile1 tile) { emptyTiles.Remove(tile); }

	private void SpawnBonuses() {
		for (int i = 0; i < emptyTiles.Count; i++)
			if (emptyTiles[i] != null && Random.Range(0, 20) == 1) {
				emptyTiles[i].SpawnBonus(Random.Range(1, 5));
				emptyTiles.Remove(emptyTiles[i]);
			}
	}

	public void GameOver() { _uiManager.GameOver(); }
}
