﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UiManager : MonoBehaviour {
	[SerializeField] private List<MenuWindow> _menus;

	[SerializeField] private Button _gameOverReturn;
	[SerializeField] private Button _menuPlay;
	[SerializeField] private Button _menuExit;
	[SerializeField] private Button _pausePlay;
	[SerializeField] private Button _pauseExit;
	[SerializeField] private Button _winReturn;

	[SerializeField] private Text _scoreText;

	private GameManager _gameManager;

	public void Create(GameManager gameManager) { _gameManager = gameManager; }

	public void Init() {
		_gameOverReturn.onClick.AddListener(ReturnToMainMenu);
		_menuPlay.onClick.AddListener(PlayGame);
		_menuExit.onClick.AddListener(ExitGame);
		_pausePlay.onClick.AddListener(PlayGame);
		_pauseExit.onClick.AddListener(ReturnToMainMenu);
		_winReturn.onClick.AddListener(ReturnToMainMenu);

		if (_gameManager.Level > 0) {
			HideAllMenus();
			Time.timeScale = 1f;
		}
		else
			Time.timeScale = 0f;
	}

	private void Update() {
		if (Input.GetButtonDown("Cancel") && _gameManager.Game) ShowPauseMenu();
	}

	private void PlayGame() {
		HideAllMenus();
		_gameManager.Game = true;
		Time.timeScale    = 1.0f;
	}

	public void WinScreen() {
		ShowMenuPanel("WinScreen", true);
		Time.timeScale = 0f;
	}

	private void ShowPauseMenu() {
		ShowMenuPanel("PauseMenu", true);
		Time.timeScale = 0f;
	}

	public void GameOver() {
		ShowMenuPanel("GameOverScreen", true);
		Time.timeScale    = 0f;
		_gameManager.Game = false;
	}

	public void ReturnToMainMenu() { SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); }

	public void HideAllMenus() { _menus.ForEach(x => x.Prefab.SetActive(false)); }

	public void ExitGame() {
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#endif
		Application.Quit();
	}

	private void ShowMenuPanel(string namePanel, bool show) {
		_menus.First(x => x.Name == namePanel).Prefab.SetActive(show);
	}

	public void SetScore(string text) { _scoreText.text = text; }
}

[Serializable]
public class MenuWindow {
	public string Name;
	public GameObject Prefab;
}
