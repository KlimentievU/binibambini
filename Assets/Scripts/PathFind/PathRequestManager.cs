﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace PathFind {
	public class PathRequestManager : MonoBehaviour {
		private Queue<PathRequest> pathRequestQueue = new Queue<PathRequest>();
		private PathRequest _currentPathRequest;

		private static PathRequestManager _instance;
		private Pathfinding _pathfinding;
		private bool _isProcessingPath;

		public void Init() {
			_instance    = this;
			_pathfinding = GetComponent<Pathfinding>();
		}

		public static void RequestPath(Vector3 pathStart, Vector3 pathEnd, Action<Vector3[], bool> callback) {
			PathRequest newRequest = new PathRequest(pathStart, pathEnd, callback);
			_instance.pathRequestQueue.Enqueue(newRequest);
			_instance.TryProcessNext();
		}

		private void TryProcessNext() {
			if (!_isProcessingPath && pathRequestQueue.Count > 0) {
				_currentPathRequest = pathRequestQueue.Dequeue();
				_isProcessingPath   = true;
				_pathfinding.StartFindPath(_currentPathRequest.pathStart, _currentPathRequest.pathEnd);
			}
		}

		public void FinishedProcessingPath(Vector3[] path, bool success) {
			_currentPathRequest.callback(path, success);
			_isProcessingPath = false;
			TryProcessNext();
		}

		private struct PathRequest {
			public Vector3 pathStart;
			public Vector3 pathEnd;
			public Action<Vector3[], bool> callback;

			public PathRequest(Vector3 start, Vector3 end, Action<Vector3[], bool> callback) {
				pathStart = start;
				pathEnd   = end;
				this.callback  = callback;
			}
		}
	}
}
