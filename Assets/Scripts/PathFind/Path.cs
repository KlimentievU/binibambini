﻿using UnityEngine;

namespace PathFind {
	public class Path {
		private readonly Vector3[] _lookPoints;
		private readonly Line[] _turnBoundaries;
		private readonly int _slowDownIndex;

		public Path(Vector3[] waypoints, Vector3 startPos, float turnDst, float stoppingDst) {
			_lookPoints     = waypoints;
			_turnBoundaries = new Line[_lookPoints.Length];
			var finishLineIndex = _turnBoundaries.Length - 1;

			Vector2 previousPoint = V3ToV2(startPos);
			for (int i = 0; i < _lookPoints.Length; i++) {
				Vector2 currentPoint      = V3ToV2(_lookPoints[i]);
				Vector2 dirToCurrentPoint = (currentPoint - previousPoint).normalized;
				Vector2 turnBoundaryPoint = (i == finishLineIndex) ? currentPoint : currentPoint - dirToCurrentPoint * turnDst;
				_turnBoundaries[i] = new Line(turnBoundaryPoint, previousPoint - dirToCurrentPoint * turnDst);
				previousPoint      = turnBoundaryPoint;
			}

			float dstFromEndPoint = 0;
			for (int i = _lookPoints.Length - 1; i > 0; i--) {
				dstFromEndPoint += Vector3.Distance(_lookPoints[i], _lookPoints[i - 1]);
				if (dstFromEndPoint > stoppingDst) {
					_slowDownIndex = i;
					break;
				}
			}
		}

		Vector2 V3ToV2(Vector3 v3) { return new Vector2(v3.x, v3.z); }

		public void DrawWithGizmos() {
			Gizmos.color = Color.black;
			foreach (Vector3 p in _lookPoints) { Gizmos.DrawCube(p + Vector3.up, Vector3.one / 10); }

			Gizmos.color = Color.white;
			foreach (Line l in _turnBoundaries) { l.DrawWithGizmos(10); }
		}
	}
}
