﻿using System.Collections;
using UnityEngine;

namespace PathFind {
	public class Unit : MonoBehaviour {
		const float MinPathUpdateTime = .2f;
		const float PathUpdateMoveThreshold = .5f;

		public Transform target;

		public float Speed = 2;
		Vector3[] _path;
		int _targetIndex;

		void Start() { StartCoroutine(UpdatePath()); }

		public void OnPathFound(Vector3[] newPath, bool pathSuccessful) {
			if (pathSuccessful) {
				_path        = newPath;
				_targetIndex = 0;
				StopCoroutine("FollowPath");
				StartCoroutine("FollowPath");
			}
		}

		protected IEnumerator UpdatePath() {
			if (Time.timeSinceLevelLoad < .3f) yield return new WaitForSeconds(.3f);
			PathRequestManager.RequestPath(transform.position, target.position, OnPathFound);

			float   sqrMoveThreshold = PathUpdateMoveThreshold * PathUpdateMoveThreshold;
			Vector3 targetPosOld     = target.position;

			while (true) {
				yield return new WaitForSeconds(MinPathUpdateTime);

				if ((target.position - targetPosOld).sqrMagnitude > sqrMoveThreshold) {
					PathRequestManager.RequestPath(transform.position, target.position, OnPathFound);
					targetPosOld = target.position;
				}
			}
		}

		IEnumerator FollowPath() {
			Vector3 currentWaypoint = _path[0];
			while (true) {
				if (transform.position == currentWaypoint) {
					_targetIndex++;
					if (_targetIndex >= _path.Length)
						yield break;
					currentWaypoint = _path[_targetIndex];
				}

				transform.LookAt(currentWaypoint);
				transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, Speed * Time.deltaTime);
				yield return null;
			}
		}

		public void OnDrawGizmos() {
			if (_path != null) {
				for (int i = _targetIndex; i < _path.Length; i++) {
					Gizmos.color = Color.black;
					Gizmos.DrawCube(_path[i], Vector3.one / 10);

					if (i == _targetIndex) { Gizmos.DrawLine(transform.position, _path[i]); }
					else { Gizmos.DrawLine(_path[i - 1],                         _path[i]); }
				}
			}
		}
	}
}
